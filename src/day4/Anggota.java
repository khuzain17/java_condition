package day4;

public class Anggota extends ClassBE {
    @Override
    public String m_anggota(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        System.out.println("nama : " + getNama() + " umur : " + getUmur());

        return "nama : " + getNama() + " umur : " + getUmur();
    }

    @Override
    public String m_judul() {
        String judul = "Ini method dari class Anggota";
        return (judul);
    }

    // multi m_anggota
    public String m_anggota(int umur, String nama) {
        this.nama = nama;
        this.umur = umur;
        return "nama : " + getNama() + " umur : " + getUmur();
    }
}