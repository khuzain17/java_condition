package day4;

public class ClassBE {
    public String nama;
    public int umur;
    public String judul = "Class BE Method final Anggota";

    public String m_judul() {
        return judul;
    }
    public String m_anggota(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        String judul = "Method m_anggota pada ClassBE";
        System.out.println(judul);
        return "nama : " + getNama() + " umur : " + getUmur() ;
    }

    public String getNama() {
        return nama;
    }
    public int getUmur() {
        return umur;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setUmur(int umur) {
        this.umur = umur;
    }
}
