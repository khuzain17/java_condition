CREATE TABLE kelasbe (
   id integer PRIMARY KEY NOT NULL,
   nama varchar(25),
   tema varchar(25),
   alamat varchar(25)
);

INSERT INTO kelasbe (id, nama, tema, alamat) 
VALUES (1,'zain', 'intro java', 'Banjarbaru'),
(2,'nana', 'tipe data', 'Jogja'),
(3,'banu', 'array', 'Malang'),
(4,'reza', 'looping', 'Jakarta'),
(5,'fara', 'for', 'Solo'),
(6,'endo', 'oop', 'Manado'),
(7,'anna', 'class', 'Kutai'),
(8,'kayla', 'method', 'Ponti'),
(9,'hana', 'database', 'Makasar'),
(10,'rikan', 'sql', 'Aceh');


SELECT * FROM kelasbe
WHERE id > 3;

UPDATE kelasbe
SET nama = 'nama telah diubah'
WHERE id = 3;

SELECT * FROM kelasbe
WHERE nama LIKE 'a%';

SELECT * FROM kelasbe
ORDER BY alamat DESC;

SELECT nama, count(nama) FROM kelasbe
GROUP BY nama;

SELECT * FROM kelasbe
WHERE id IN (3,4);