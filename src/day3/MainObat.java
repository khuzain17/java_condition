package day3;

public class MainObat {
    public static void main(String[] args) {
        Obat obat = new Obat();
        obat.setId(1);
        obat.setNamaObat("Bodrex");
        obat.setHargaObat(100);

        System.out.println("Id : "+ obat.getId());
        System.out.println("Nama Obat : "+ obat.getNamaObat());
        System.out.println("Harga Obat : "+ obat.getHargaObat());
    }

}
