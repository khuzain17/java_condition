package day3;

public class Obat {
    long id;
    String namaObat;
    double hargaObat;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getNamaObat()
    {
        return namaObat;
    }

    public void setNamaObat(String namaObat)
    {
        this.namaObat = namaObat;
    }

    public double getHargaObat()
    {
        return hargaObat;
    }

    public void setHargaObat(double hargaObat)
    {
        this.hargaObat = hargaObat;
    }
}
