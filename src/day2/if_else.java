package day2;

import java.util.Scanner;

public class if_else {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter provinsi");
        String provinsi = myObj.nextLine();

        System.out.println("Enter kota");
        String kota = myObj.nextLine();

        System.out.println("Enter Jumlah penduduk");
        int jumlahPenduduk = myObj.nextInt();

        System.out.println("provinsi is: " + provinsi);
        System.out.println("kota is: " + kota);
        System.out.println("Jumlah penduduk is: " + jumlahPenduduk);


        if (provinsi.equals("aceh") && kota.equals("Kabupaten Aceh Barat")) {
            System.out.println("Buah pisang");
        } else if (provinsi.equals("aceh") && kota.equals("Kabupaten Aceh Besar")) {
            for (int i = 1; i < 11; i++) {
                System.out.println(i);
            }
        } else if (provinsi.equals("jambi") && kota.equals("Kabupaten Batanghari") && jumlahPenduduk > 1000) {
            System.out.println("Jumlah penduduk kabupaten batanghari > 1000");
        } else if (provinsi.equals("jambi") && kota.equals("Kabupaten Batanghari") && jumlahPenduduk < 1000) {
            System.out.println("Jumlah penduduk kabupaten batanghari < 1000");
        } else if (provinsi.equals("jambi") && kota.equals("Kabupaten Bungo")) {
            for (int i = 1; i < 11; i++) {
                if (i == 5) {
                    break;
                }
                System.out.println(i);
            }
        } else if (provinsi.equals("lampung") && kota.equals("Kabupaten Lampung Timur")) {
            System.out.println("Buah apel");
        } else if (provinsi.equals("lampung") && kota.equals("Kabupaten Lampung Utara")) {
            System.out.println("Buah apel");
        } else if (provinsi.equals("bali") && kota.equals("Kabupaten Bangli") && jumlahPenduduk > 1000) {
            System.out.println("Jumlah penduduk kabupaten Bangli > 1000");
        } else if (provinsi.equals("bali") && kota.equals("Kabupaten Buleleng") && jumlahPenduduk > 1000) {
            System.out.println("Jumlah penduduk kabupaten Buleleng> 1000");
        } else if (provinsi.equals("papua") && kota.equals("Kabupaten Asmat")) {
            for (int i = 30; i < 41; i++) {
                System.out.println(i);
            }
        } else if (provinsi.equals("papua") && kota.equals("Kabupaten Biak Numfor")) {
            for (int i = 30; i < 41; i++) {
                System.out.println(i);
            }
        } else {
            System.out.println("Hello World!");
        }
    }

}
